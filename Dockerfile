
# Use an official Node.js runtime as a parent image
FROM node:18

# Create app directory
WORKDIR /usr/src/app


# Install app dependencies
 COPY package*.json app.js ./

RUN npm install

 
COPY . .
EXPOSE 3000


# Command to run your application
CMD ["npm", "start"]


